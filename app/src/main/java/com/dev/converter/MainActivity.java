package com.dev.converter;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FieldsUpdater fieldsUpdater = new FieldsUpdater(this);

        MetricChoiceHandler metricsHandler = new MetricChoiceHandler(this, fieldsUpdater);
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(metricsHandler);

        EditText inputField = (EditText) findViewById(R.id.inputField);
        InputNumberHandler handler = new InputNumberHandler(this, fieldsUpdater);
        inputField.addTextChangedListener(handler);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        String language = "en";
        if (id == R.id.rus_settings) {
            language = "ru";
        } else if (id == R.id.french_settings) {
            language = "fr";
        }
        changeLocale(getApplicationContext().getResources(), language);
        return super.onOptionsItemSelected(item);
    }

    private void changeLocale(Resources resource, String locale) {
        Configuration config = new Configuration(resource.getConfiguration());
        config.locale = new Locale(locale);
        resource.updateConfiguration(config, resource.getDisplayMetrics());

        ((RadioButton) findViewById(R.id.metersRB)).setText(R.string.meters);
        ((RadioButton) findViewById(R.id.kmRB)).setText(R.string.km);
        ((RadioButton) findViewById(R.id.cmRB)).setText(R.string.sm);
        ((RadioButton) findViewById(R.id.milesRB)).setText(R.string.miles);
        ((RadioButton) findViewById(R.id.ftRB)).setText(R.string.ft);
        ((RadioButton) findViewById(R.id.inchesRB)).setText(R.string.inch);

        ((TextView) findViewById(R.id.metersTV)).setText(R.string.meters);
        ((TextView) findViewById(R.id.kmTV)).setText(R.string.km);
        ((TextView) findViewById(R.id.cmTV)).setText(R.string.sm);
        ((TextView) findViewById(R.id.milesTV)).setText(R.string.miles);
        ((TextView) findViewById(R.id.ftTV)).setText(R.string.ft);
        ((TextView) findViewById(R.id.inchesTV)).setText(R.string.inch);
    }
}
