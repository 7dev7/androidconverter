package com.dev.converter;

import java.math.BigDecimal;

public interface CallbackUpdater {
    void update(BigDecimal value, MetricsType type);

    void resetToDefault();
}
