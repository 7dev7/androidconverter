package com.dev.converter;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.math.BigDecimal;


public class InputNumberHandler implements TextWatcher {

    private Activity activity;
    private CallbackUpdater updater;

    public InputNumberHandler(Activity activity, CallbackUpdater updater) {
        this.activity = activity;
        this.updater = updater;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //NOP
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.toString().isEmpty()) {
            updater.resetToDefault();
            return;
        }
        String val = s.toString();
        if (s.toString().startsWith(".")) {
            val = "0" + s.toString();
        }
        BigDecimal value = new BigDecimal(val);

        RadioGroup radioGroup = (RadioGroup) activity.findViewById(R.id.radioGroup);
        RadioButton radioButton = (RadioButton) activity.findViewById(radioGroup.getCheckedRadioButtonId());
        int index = radioGroup.indexOfChild(radioButton);

        MetricsType type = MetricsType.values()[index];

        updater.update(value, type);
    }

    @Override
    public void afterTextChanged(Editable s) {
        //NOP
    }
}
