package com.dev.converter;

public enum MetricsType {
    METERS,
    KM,
    CM,
    FT,
    MILES,
    INCHES,
}
