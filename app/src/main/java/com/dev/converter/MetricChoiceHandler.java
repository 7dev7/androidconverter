package com.dev.converter;

import android.app.Activity;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.math.BigDecimal;


public class MetricChoiceHandler implements RadioGroup.OnCheckedChangeListener {

    private Activity activity;
    private CallbackUpdater updater;

    public MetricChoiceHandler(Activity activity, CallbackUpdater updater) {
        this.activity = activity;
        this.updater = updater;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton metric = (RadioButton) activity.findViewById(checkedId);
        int selectedRBIndex = group.indexOfChild(metric);
        MetricsType type = MetricsType.values()[selectedRBIndex];

        String valueStr = ((EditText) activity.findViewById(R.id.inputField)).getText().toString();
        if (valueStr.trim().isEmpty()) {
            updater.resetToDefault();
            return;
        }
        String val = valueStr;
        if (valueStr.startsWith(".")) {
            val = "0" + valueStr;
        }
        BigDecimal value = new BigDecimal(val);

        updater.update(value, type);
    }
}
