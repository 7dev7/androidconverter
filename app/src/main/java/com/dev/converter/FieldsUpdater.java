package com.dev.converter;

import android.app.Activity;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;


public class FieldsUpdater implements CallbackUpdater {
    private TextView kmField;
    private TextView mField;
    private TextView smField;
    private TextView milesField;
    private TextView ftField;
    private TextView inchesField;

    public FieldsUpdater(Activity activity) {
        kmField = (TextView) activity.findViewById(R.id.kmField);
        mField = (TextView) activity.findViewById(R.id.metersField);
        smField = (TextView) activity.findViewById(R.id.smField);
        milesField = (TextView) activity.findViewById(R.id.milesField);
        ftField = (TextView) activity.findViewById(R.id.ftField);
        inchesField = (TextView) activity.findViewById(R.id.inchesField);
    }

    @Override
    public void update(BigDecimal value, MetricsType type) {
        updateKm(value, type);
        updateM(value, type);
        updateSm(value, type);
        updateFt(value, type);
        updateMiles(value, type);
        updateInches(value, type);
    }

    @Override
    public void resetToDefault() {
        BigDecimal newVal = BigDecimal.ZERO;
        kmField.setText(newVal.toString());
        mField.setText(newVal.toString());
        smField.setText(newVal.toString());
        milesField.setText(newVal.toString());
        ftField.setText(newVal.toString());
        inchesField.setText(newVal.toString());
    }

    private void updateKm(BigDecimal value, MetricsType type) {
        BigDecimal newVal = BigDecimal.ZERO;
        switch (type) {
            case METERS: newVal = value.divide(BigDecimal.valueOf(1000L)); break;
            case KM: newVal = value; break;
            case CM: newVal = value.divide(BigDecimal.valueOf(100000L)); break;
            case FT: newVal = value.multiply(BigDecimal.valueOf(0.0003048)); break;
            case MILES: newVal = value.multiply(BigDecimal.valueOf(1.60934)); break;
            case INCHES: newVal = value.multiply(BigDecimal.valueOf(0.0000254)); break;
        }
        printValue(newVal, kmField);
    }

    private void updateM(BigDecimal value, MetricsType type) {
        BigDecimal newVal = BigDecimal.ZERO;
        switch (type) {
            case METERS: newVal = value; break;
            case KM: newVal =  value.multiply(BigDecimal.valueOf(1000L)); break;
            case CM: newVal = value.divide(BigDecimal.valueOf(100L)); break;
            case FT: newVal = value.multiply(BigDecimal.valueOf(0.3048)); break;
            case MILES: newVal = value.multiply(BigDecimal.valueOf(1609.34)); break;
            case INCHES: newVal = value.multiply(BigDecimal.valueOf(0.0254)); break;
        }
        printValue(newVal, mField);
    }

    private void updateSm(BigDecimal value, MetricsType type) {
        BigDecimal newVal = BigDecimal.ZERO;
        switch (type) {
            case METERS: newVal = value.multiply(BigDecimal.valueOf(100L)); break;
            case KM: newVal =  value.multiply(BigDecimal.valueOf(100000L)); break;
            case CM: newVal = value; break;
            case FT: newVal = value.multiply(BigDecimal.valueOf(30.48)); break;
            case MILES: newVal = value.multiply(BigDecimal.valueOf(160934L)); break;
            case INCHES: newVal = value.multiply(BigDecimal.valueOf(2.54)); break;
        }
        printValue(newVal, smField);
    }

    private void updateFt(BigDecimal value, MetricsType type) {
        BigDecimal newVal = BigDecimal.ZERO;
        switch (type) {
            case METERS: newVal = value.multiply(BigDecimal.valueOf(3.28)); break;
            case KM: newVal =  value.multiply(BigDecimal.valueOf(3280.84)); break;
            case CM: newVal = value.multiply(BigDecimal.valueOf(0.0328084)); break;
            case FT: newVal = value; break;
            case MILES: newVal = value.multiply(BigDecimal.valueOf(5280)); break;
            case INCHES: newVal = value.multiply(BigDecimal.valueOf(0.0833333)); break;
        }
        printValue(newVal, ftField);
    }

    private void updateMiles(BigDecimal value, MetricsType type) {
        BigDecimal newVal = BigDecimal.ZERO;
        switch (type) {
            case METERS: newVal = value.multiply(BigDecimal.valueOf(0.000621371)); break;
            case KM: newVal =  value.multiply(BigDecimal.valueOf(0.621371)); break;
            case CM: newVal = value.multiply(BigDecimal.valueOf(0.0000062137)); break;
            case FT: newVal = value.multiply(BigDecimal.valueOf(0.000189394)); break;
            case MILES: newVal = value; break;
            case INCHES: newVal = value.multiply(BigDecimal.valueOf(0.000015782822)); break;
        }
        printValue(newVal, milesField);
    }

    private void updateInches(BigDecimal value, MetricsType type) {
        BigDecimal newVal = BigDecimal.ZERO;
        switch (type) {
            case METERS: newVal = value.multiply(BigDecimal.valueOf(39.3701)); break;
            case KM: newVal =  value.multiply(BigDecimal.valueOf(39370.1)); break;
            case CM: newVal = value.multiply(BigDecimal.valueOf(0.393701)); break;
            case FT: newVal = value.multiply(BigDecimal.valueOf(12L)); break;
            case MILES: newVal = value.multiply(BigDecimal.valueOf(63360L)); break;
            case INCHES: newVal = value; break;
        }
        printValue(newVal, inchesField);
    }

    private void printValue(BigDecimal value, TextView textView) {
        if (value.precision() > 10 || value.precision() < -10) {
            textView.setText(format(value, 2));
        } else {
            textView.setText(value.toString());
        }
    }

    private String format(BigDecimal x, int scale) {
        NumberFormat formatter = new DecimalFormat("00.00E00");
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        formatter.setMinimumFractionDigits(scale);
        return formatter.format(x);
    }
}
